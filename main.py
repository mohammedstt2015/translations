import os
import csv
import json
import codecs
import argparse

args = argparse.Namespace()
languages = ['ja', 'zh', 'en', 'ko']


def parse():
    parser = argparse.ArgumentParser()
    parser.add_argument(
        'command', help='one of <pull> <push>')
    parser.add_argument(
        '--db', default='../database/database/', help='path of database')
    return parser.parse_args()


def load_csv(file_name):
    data = {}
    try:
        with open(file_name) as f:
            if f.read(1).encode() != codecs.BOM_UTF8:
                f.seek(0)
            csvFile = csv.reader(f)
            for row in csvFile:
                if csvFile.line_num == 1:
                    header = row
                else:
                    data[row[0]] = dict(zip(header, row))
    except OSError:
        pass
    return data


def pull():
    names = set()
    with open('config.json') as f:
        config = json.load(f)
    for file in config:
        with open(args.db + file['name'] + '.json') as f:
            data = json.load(f)
        for key in file['keys']:
            trans = load_csv('trans/%s %s.csv' % (file['name'], key))
            names.update(trans.keys())
            for item in data:
                if item[key] in names:
                    continue
                names.add(item[key])
                trans[item[key]] = {languages[0]: item[key]}
            with open('trans/%s %s.csv' % (file['name'], key), 'wb') as f:
                f.write(codecs.BOM_UTF8)
            with open('trans/%s %s.csv' % (file['name'], key), 'a') as f:
                csvFile = csv.DictWriter(f, languages)
                csvFile.writeheader()
                csvFile.writerows(trans.values())


def push():
    with open('config.json') as f:
        config = json.load(f)
    packages = {language: {} for language in languages[1:]}
    for file in config:
        for key in file['keys']:
            trans = load_csv('trans/%s %s.csv' % (file['name'], key))
            for tran in trans.values():
                for language in languages[1:]:
                    if language in tran and tran[language]:
                        packages[language][tran[languages[0]]] = tran[language]
    os.makedirs('public', exist_ok=True)
    for language in packages:
        with open('public/%s.json' % language, 'w') as f:
            json.dump(packages[language], f,
                      ensure_ascii=False, sort_keys=True)


def main():
    global args
    args = parse()

    if args.command == 'pull':
        pull()
    elif args.command == 'push':
        push()


if __name__ == '__main__':
    main()
